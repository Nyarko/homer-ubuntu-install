# Homer on Ubuntu #

This script is an edited version of the original script that can be found at
 https://github.com/sipcapture/homer  

 The credits and notice for the original script is:
```
 --------------------------------------------------------------------------------
 HOMER/SipCapture automated installation script for Debian/CentOs/OpenSUSE (BETA)
 --------------------------------------------------------------------------------
 This script is only intended as a quickstart to test and get familiar with HOMER.
 It is not suitable for high-traffic nodes, complex capture scenarios, clusters.
 The HOW-TO should be ALWAYS followed for a fully controlled, manual installation!
 --------------------------------------------------------------------------------

  Copyright notice:

  (c) 2011-2016 Lorenzo Mangani <lorenzo.mangani@gmail.com>
  (c) 2011-2016 Alexandr Dubovikov <alexandr.dubovikov@gmail.com>

  All rights reserved

  This script is part of the HOMER project (http://sipcapture.org)
  The HOMER project is free software; you can redistribute it and/or 
  modify it under the terms of the GNU Affero General Public License as 
  published by the Free Software Foundation; either version 3 of 
  the License, or (at your option) any later version.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>

  This script is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  This copyright notice MUST APPEAR in all copies of the script!
  ```
 
 For more information on Homer the Sipcapture project please visit http://sipcapture.org